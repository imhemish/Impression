<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
    <id>@app-id@</id>

    <name>Impression</name>
    <developer_name>Khaleel Al-Adhami</developer_name>
    <summary>Create bootable drives</summary>

    <metadata_license>CC-BY-SA-4.0</metadata_license>
    <project_license>GPL-3.0-only</project_license>

    <description>
        <p>
            Flash disk images onto your drives with ease. Select an image, insert your drive, and you're good to go! Impression is a useful tool for both avid distro-hoppers and casual computer users.
        </p>
    </description>

    <translation type="gettext">@gettext-package@</translation>

    <categories>
        <category>System</category>
        <category>Utility</category>
        <category>GTK</category>
        <category>GNOME</category>
    </categories>

    <keywords>
        <keyword>usb</keyword>
        <keyword>flash</keyword>
        <keyword>bootable</keyword>
        <keyword>drive</keyword>
        <keyword>iso</keyword>
        <keyword>disk</keyword>
        <keyword>image</keyword>
    </keywords>

    <requires>
        <internet>offline-only</internet>
        <display_length compare="ge">360</display_length>
    </requires>

    <recommends>
        <control>pointing</control>
        <control>keyboard</control>
        <control>touch</control>
    </recommends>

    <screenshots>
        <screenshot>
            <image>https://gitlab.com/adhami3310/Impression/-/raw/main/data/resources/screenshots/0.png</image>
            <caption>Main screen with a chosen ISO and available USB memories</caption>
        </screenshot>
        <screenshot>
            <image>https://gitlab.com/adhami3310/Impression/-/raw/main/data/resources/screenshots/1.png</image>
            <caption>Flashing the ISO in progress</caption>
        </screenshot>
        <screenshot>
            <image>https://gitlab.com/adhami3310/Impression/-/raw/main/data/resources/screenshots/2.png</image>
            <caption>Success screen with a big check mark</caption>
        </screenshot>
    </screenshots>

    <launchable type="desktop-id">@app-id@.desktop</launchable>

    <url type="homepage">https://gitlab.com/adhami3310/Impression</url>
    <url type="bugtracker">https://gitlab.com/adhami3310/Impression/-/issues</url>
    <url type="help">https://gitlab.com/adhami3310/Impression/-/issues</url>
    <url type="vcs-browser">https://gitlab.com/adhami3310/Impression</url>
    <url type="contribute">https://gitlab.com/adhami3310/Impression</url>
    <url type="contact">https://matrix.to/#/@adhami:matrix.org</url>

    <update_contact>khaleel.aladhami@gmail.com</update_contact>

    <content_rating type="oars-1.1" />

    <releases>
        <release version="2.1" date="2023-06-20">
          <description>
            <p>This minor release of Impression delivers:</p>
            <ul>
              <li>Support for mobile screen sizes</li>
              <li>Various bug fixes, improving reliability and stability</li>
              <li>Brazillian Portugese translations, making Impression available in a total of 9 languages</li>
            </ul>
            <p>Impression is made possible by volunteer developers, designers, and translators. Thank you for your contributions!</p>
          </description>
        </release>
        <release version="2.0" date="2023-06-13">
          <description>
            <p>This major release of Impression brings a bunch of exciting improvements:</p>
            <ul>
              <li>Visual enhancements to make the app more beautiful, focused, and engaging</li>
              <li>Automatic updates of the available drives list</li>
              <li>Explicit drive selection before flashing, to avoid accidental data loss</li>
              <li>Turkish and Czech translations, making Impression available in a total of 8 languages</li>
            </ul>
            <p>The versioning scheme has been simplified to only include major and minor versions. The previous version of Impression was 1.0.1,
              this is version 2.0. Going forward, new features and noticeable changes will be included in
              new major releases, while fixes and translations will result in new minor releases.</p>
            <p>Impression is made possible by volunteer developers, designers, and translators. Thank you for your contributions!</p>
          </description>
        </release>
        <release version="1.0.1" date="2023-06-07">
            <description>
                <p>Added Spanish, French, German, Russian, and Italian translations.</p>
            </description>
        </release>
        <release version="1.0.0" date="2023-06-03">
            <description>
                <p>Initial version.</p>
            </description>
        </release>
    </releases>
</component>
